package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Review {
    public Review() {

    }



    public Review(String roommateId, String review) {

        this.roommateId = roommateId;

        this.review = review;

    }



    private String roommateId;

    private String review; // 0 to 5
}
