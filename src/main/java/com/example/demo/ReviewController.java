package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/review")

public class ReviewController {



    @GetMapping("/{roommateId}")

    public Review getReviewByRoommateId(

            @PathVariable("roommateId") String roommateId) {



        return new Review(roommateId, "Nice roommie");

    }

}
